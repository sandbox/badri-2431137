<?php

/**
 * @file
 * Contains \Drupal\twitter\Form\TwitterAdminForm.
 */

namespace Drupal\twitter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class TwitterAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'twitter_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('twitter.settings');

    foreach (Element::children($form) as $variable) {
      $config->set($variable, $form_state->getValue($form[$variable]['#parents']));
    }
    $config->save();

    if (method_exists($this, '_submitForm')) {
      $this->_submitForm($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['twitter.settings'];
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface &$form_state) {
    $form['twitter_import'] = [
      '#type' => 'checkbox',
      '#title' => t('Import and display the Twitter statuses of site users who have entered their Twitter account information.'),
      '#default_value' => variable_get('twitter_import', 1),
    ];
    $form['twitter_expire'] = [
      '#type' => 'select',
      '#title' => t('Delete old statuses'),
      '#default_value' => variable_get('twitter_expire', 0),
      '#options' => [
        0 => t('Never')
        ] + drupal_map_assoc([604800, 2592000, 7776000, 31536000], 'format_interval'),
      '#states' => [
        'visible' => [
          ':input[name=twitter_import]' => [
            'checked' => TRUE
            ]
          ]
        ],
    ];

    $form['oauth'] = [
      '#type' => 'fieldset',
      '#title' => t('OAuth Settings'),
      '#access' => module_exists('oauth_common'),
      '#description' => t('To enable OAuth based access for twitter, you must <a href="@url">register your application</a> with Twitter and add the provided keys here.', [
        '@url' => 'https://dev.twitter.com/apps/new'
        ]),
    ];
    $form['oauth']['callback_url'] = [
      '#type' => 'item',
      '#title' => t('Callback URL'),
      '#markup' => url('twitter/oauth', [
        'absolute' => TRUE
        ]),
    ];
    $form['oauth']['twitter_consumer_key'] = [
      '#type' => 'textfield',
      '#title' => t('OAuth Consumer key'),
      '#default_value' => variable_get('twitter_consumer_key', NULL),
    ];
    $form['oauth']['twitter_consumer_secret'] = [
      '#type' => 'textfield',
      '#title' => t('OAuth Consumer secret'),
      '#default_value' => variable_get('twitter_consumer_secret', NULL),
    ];

    // Twitter external APIs settings.
    $form['twitter'] = [
      '#type' => 'fieldset',
      '#title' => t('Twitter Settings'),
      '#description' => t('The following settings connect Twitter module with external APIs. ' . 'Change them if, for example, you want to use Identi.ca.'),
    ];
    $form['twitter']['twitter_host'] = [
      '#type' => 'textfield',
      '#title' => t('Twitter host'),
      '#default_value' => variable_get('twitter_host', TWITTER_HOST),
    ];
    $form['twitter']['twitter_api'] = [
      '#type' => 'textfield',
      '#title' => t('Twitter API'),
      '#default_value' => variable_get('twitter_api', TWITTER_API),
    ];
    $form['twitter']['twitter_search'] = [
      '#type' => 'textfield',
      '#title' => t('Twitter search'),
      '#default_value' => variable_get('twitter_search', TWITTER_SEARCH),
    ];
    $form['twitter']['twitter_tinyurl'] = [
      '#type' => 'textfield',
      '#title' => t('TinyURL'),
      '#default_value' => variable_get('twitter_tinyurl', TWITTER_TINYURL),
    ];

    return parent::buildForm($form, $form_state);
  }

}
