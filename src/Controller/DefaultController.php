<?php /**
 * @file
 * Contains \Drupal\twitter\Controller\DefaultController.
 */

namespace Drupal\twitter\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Default controller for the twitter module.
 */
class DefaultController extends ControllerBase {

  public function twitter_user_settings($account = NULL) {
    // Verify OAuth keys.
    if (!twitter_api_keys()) {
      $variables = [
        '@twitter-settings' => url('admin/config/services/twitter/settings')
        ];
      $output = '<p>' . t('You need to authenticate at least one Twitter account in order to use the Twitter API. Please fill out the OAuth fields at <a href="@twitter-settings">Twitter Settings</a> and then return here.', $variables) . '</p>';
    }
    else {
      module_load_include('inc', 'twitter');
      if (!$account) {
        $twitter_accounts = twitter_load_accounts();
      }
      else {
        $twitter_accounts = twitter_twitter_accounts($account);
      }

      $output = [];
      if (count($twitter_accounts)) {
        // List Twitter accounts.
        $output['header']['#markup'] = '<p>';
        if (user_access('administer site configuration')) {
          $variables = [
            '@run-cron' => url('admin/reports/status/run-cron', [
              'query' => [
                'destination' => 'admin/config/services/twitter'
                ]
              ])
            ];
          $output['header']['#markup'] .= t('Tweets are pulled from Twitter by <a href="@run-cron">running cron</a>.', $variables) . ' ';
        }
        $variables = ['@tweets' => url('tweets')];
        $output['header']['#markup'] .= t('You can view the full list of tweets at the <a href="@tweets">Tweets</a> view.', $variables);
        $output['header']['#markup'] .= '</p>';
        $output['list_form'] = drupal_get_form('twitter_account_list_form', $twitter_accounts);
      }
      else {
        // No accounts added. Inform about how to add one.
        $output['header'] = [
          '#markup' => '<p>' . t('No Twitter accounts have been added yet. Click on the following button to add one.') . '</p>'
          ];
      }

      $output['add_account'] = [
        '#type' => 'fieldset',
        '#title' => t('Add Twitter accounts'),
        '#weight' => 5,
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      ];

      if (user_access('add authenticated twitter accounts')) {
        $output['add_account']['form'] = drupal_get_form('twitter_auth_account_form');
      }
      if (twitter_connect()) {
        $output['add_account']['non_auth'] = drupal_get_form('twitter_non_auth_account_form');
      }
    }

    // Give a chance to other modules to alter the output.
    drupal_alter('twitter_user_settings', $output);

    return $output;
  }

  public function twitter_account_access(Drupal\Core\Session\AccountInterface $account) {
    return user_access('add twitter accounts') || user_access('add authenticated twitter accounts');
  }

}
