<?php

/**
 * @file
 * Contains Drupal\twitter\TwitterService.
 */

namespace Drupal\twitter;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Database\Driver\mysql\Connection;

class TwitterService
{
  /**
   * Drupal\Core\Config\ConfigFactory definition.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $config_factory;

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  public function __construct(ConfigFactory $config_factory, Connection $database) {
    $this->config_factory = $config_factory;
    $this->database = $database;

    $twitter_config = $this->configFactory->get('twitter.settings');

    $settings = array(
      'oauth_access_token' => $this->twitter_config->get(''),
      'oauth_access_token_secret' => $this->twitter_config->get(''),
      'consumer_key' => $this->twitter_config->get(''),
      'consumer_secret' => $this->twitter_config->get(''),
    );

    $twitter = new \TwitterAPIExchange($settings);
  }
}
